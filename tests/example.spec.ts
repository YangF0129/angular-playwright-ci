import { test, expect } from '@playwright/test';

test('app has title "AngularPlaywrightCi"', async ({ page }) => {
  await page.goto('http://localhost:4200');

  await expect(page).toHaveTitle('AngularPlaywrightCi');
});

// test('should fail', async ({ page }) => {
//   await page.goto('http://localhost:4200');

//   await expect(page).toHaveTitle('tata');
// });
