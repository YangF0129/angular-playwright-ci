import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'angular-playwright-ci';
  firstMockServerData$!: Observable<{
    id: number;
    firstName: string;
    lastName: string;
  }>;
  secondMockServerData$!: Observable<{
    id: number;
    firstName: string;
    lastName: string;
  }>;

  constructor(private httpClient: HttpClient) {}

  ngOnInit() {
    this.firstMockServerData$ = this.httpClient.get<{
      id: number;
      firstName: string;
      lastName: string;
    }>('http://localhost:8081/rest/user');
    this.secondMockServerData$ = this.httpClient.get<{
      id: number;
      firstName: string;
      lastName: string;
    }>('http://localhost:8080/rest/user');
  }
}
