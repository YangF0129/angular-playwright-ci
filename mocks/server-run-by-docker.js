const express = require("express");
const cors = require("cors");
const app = express();

app.use(cors());

app.get("/rest/healthcheck", function (req, res, next) {
  res.send(200);
});

app.get("/rest/user", function (req, res, next) {
  res.json({ id: 1, firstName: "Fifi", lastName: "Duck" });
});

app.listen(81, function () {
  console.log("CORS-enabled web server listening on port 81");
});
