# java + wiremock + playwright + node
FROM wiremock/wiremock AS wiremock

FROM mcr.microsoft.com/playwright:v1.34.3-jammy

COPY --from=wiremock /var/wiremock/lib/wiremock-jre8-standalone.jar wiremock-jre8-standalone.jar
RUN apt update && apt install default-jre -y

EXPOSE 8080
